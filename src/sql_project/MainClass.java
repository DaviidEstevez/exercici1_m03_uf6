/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql_project;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.ResultSet;
import java.util.ArrayList;
/**
 *
 * @author David
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConfigDataDB connection = new ConfigDataDB("jdbc:oracle:thin:@localhost:1521/DAVIDDB", "DANAYA_EMPRESA", "1234");
        List<Persona> personas = SelectInfo("SELECT * FROM PERSONA", connection);
        
        for (Persona persona : personas) {
            System.out.println(persona.toString());
        }
    }
    
    public static List<Persona> SelectInfo(String sentencia, ConfigDataDB connection){
        List<Persona> personas = new ArrayList<Persona>();
        try {

            Connection con = connection.start_connection();
            
            Statement statement = con.createStatement();
            ResultSet resultado = statement.executeQuery(sentencia);
            
            while ( resultado.next() )
            {
                Persona persona = new Persona(resultado.getInt(1), resultado.getString(2));   
                personas.add(persona);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return personas;
    }
    
}
