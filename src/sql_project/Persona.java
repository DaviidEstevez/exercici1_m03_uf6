/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql_project;

/**
 *
 * @author David
 */
public class Persona {
    
    private int id;
    private String nom;

    public Persona(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nom=" + nom + '}';
    }    
    
}
