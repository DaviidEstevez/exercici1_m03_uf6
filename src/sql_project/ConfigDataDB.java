/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql_project;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author David
 */
public class ConfigDataDB {
    
    private String URL, USER, PASSW;
    
    public ConfigDataDB() {
    }

    public ConfigDataDB(String URL, String USER, String PASSW) {
        this.URL = URL;
        this.USER = USER;
        this.PASSW = PASSW;
    }
    
    public Connection start_connection(){  
        Connection con = null;
        try {
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            
            con = DriverManager.getConnection(URL,USER ,PASSW);
            
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDataDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConfigDataDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return con;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getUSER() {
        return USER;
    }

    public void setUSER(String USER) {
        this.USER = USER;
    }

    public String getPASSW() {
        return PASSW;
    }

    public void setPASSW(String PASSW) {
        this.PASSW = PASSW;
    }
    
    
    
}
